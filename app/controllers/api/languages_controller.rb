class Api::LanguagesController < ApplicationController
  def index
    if q = params[:queries] && params[:queries]["search"]
      @language_relevances = LanguagesSearchEngine.search q
      @languages_with_precision =
        @language_relevances.map do |lr|
          {
            "name": lr.language.name,
            "types": lr.language.types,
            "designers": lr.language.designers,
            "precision": lr.precision
          }
        end
      render json: {"records": @languages_with_precision}
    else
      render json: {"error": "Please provide a search paramter", "data": []}
    end
  end
end
