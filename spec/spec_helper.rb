ENV['RACK_ENV'] = 'test'
require 'rack/test'
require 'pry'

SPEC_ROOT = Pathname(__FILE__).dirname
Dir[SPEC_ROOT.join("../lib/*.rb").to_s].each(&method(:require))

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
  config.shared_context_metadata_behavior = :apply_to_host_groups
  config.order = :random
  Kernel.srand config.seed
end
