RSpec.describe :languages do
  describe "Do a search" do
    before :each do
      subject
    end

    context "#new" do
      subject do
        LanguagesSearchEngine
      end

      it "loads database successfully" do
        expect(subject.db_accessible?).to be true
      end
    end

    context "#search" do
      subject do
        LanguagesSearchEngine
      end

      it "performs a correct per-word mathicng query" do
        expect(subject.search("lisp common").map(&:language).map(&:name)).to match_array ["Common Lisp"]
      end

      it "performs a correct query returning more than one language" do
        expect(subject.search("Visual").map(&:language).map(&:name)).to match_array ["Visual Basic", "Visual FoxPro"]
      end

      it "performs a exact match query" do
        expect(subject.search("Scripting Microsoft").map(&:language).map(&:name)).to match_array ["Windows PowerShell", "JScript", "VBScript"]
      end

      it "performs a partial match (2 of 3 match)" do
        expect(subject.search("Compiled Microsoft Visual").map(&:language).map(&:name)).to match_array ["C#", "Visual FoxPro", "Visual Basic", "X++"]
      end

      it "returns a partial match query results with with an order by precision descending" do
        expect(subject.search("Compiled Microsoft Visual").map(&:language).map(&:name)).to eq ["Visual FoxPro", "Visual Basic", "X++", "C#"]
      end

      it "supports a negative match" do
        expect(subject.search("Compiled Microsoft Visual -Basic").map(&:language).map(&:name)).to match_array ["C#", "Visual FoxPro", "X++"]
      end
    end
  end
end
