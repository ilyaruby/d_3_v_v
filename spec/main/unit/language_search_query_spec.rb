RSpec.describe :language_search_query do
  describe "Parses a text query string" do
    before :each do
      subject
    end

    context "#exact_match_queries" do
      subject do
        LanguagesSearchQuery.new 'aaa "bbb ccc" ddd "-eee" fff -ggg hhh -iii'
      end

      it "extracts exact match queries" do
        expect(subject.exact_match_queries).to eq ["bbb ccc", "-eee"]
      end

      it "extracts also regular word match queries" do
        expect(subject.word_match_queries).to eq %w[aaa ddd fff hhh]
      end

      it "extracts also negative match queries" do
        expect(subject.negative_match_queries).to eq %w[ggg iii]
      end
    end
  end
end
