RSpec.describe :language do
  describe "Do a name matching" do
    before :each do
      subject
    end

    subject do
      Language.new "Common Lisp", "Scripting", "Microsoft"
    end

    context "#matches?" do
      it "exact matching" do
        expect(subject.relevance("Common Lisp").precision).to be > 0.5
      end

      it "case-insensitive matching" do
        expect(subject.relevance("common lisp").precision).to be > 0.5
      end

      it "partial matching by words" do
        expect(subject.relevance("common").precision).to be > 0.5
      end

      it "word order does not matters" do
        expect(subject.relevance("Lisp Common").precision).to be > 0.5
      end

      it "should match in different fields" do
        expect(subject.relevance("Scripting Microsoft").precision).to be > 0.5
      end

      it "should not match if one query word is not included in a name" do
        expect(subject.relevance("Something Common").precision).to be <= 0.5
      end
    end

    context "#relevance_precision" do
      it "word order does not matters" do
        expect(subject.relevance("Uncommon Lisp").precision).to eq 0.5
      end
    end
  end
end
