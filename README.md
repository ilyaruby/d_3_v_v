# What's this?

Some test work done for one company.

I will not disclose it's name here because one of the requirements was to make this code non-googleable.

# Code run requirements

This code is intended to run on Ruby 2.2.3.

# How to run it

Start it as a regular rails app. Web interface will be available at "http://127.0.0.1:3000/".

# Querying

Just type some words. It uses a simple "database" with languages info in it.

The logic is:

* It searches for words in a query in all language info fields
* If more than 50% query words are there, the language is selected.
* If negate query (e.g. "-Basic") is met, language will not be selected
* It also supports exact matches (e.g. ' "Visual Basic" ')
* Relevance precision value is also returned
* Records are sorted by a backend based on that value descending

# Architecture

This is a Rails backend with some JS/AJAX frontend representing results.

Searching is done in backend.

Code consist of three parts:

* public/index.html containing a frontend
* lib/ containing a backend classes
* regular Rails boilerplate & some glue to make former work

Backend classes are made separate for reusability.
They could be packaged as a gem and/or be used with any framework (e.g. Sinatra, Roda).

# Implementation notes

I'd recommend not to start a project with an API this simple as a Rails monolith.

This code uses styles and scripts from a dynatable.com CDN (also jQuery and some other). This is to make evaluation of this code easier not a recommended way to do things in a production environment.

This code is covered by unit tests, I've used TDD methodology from the start.
