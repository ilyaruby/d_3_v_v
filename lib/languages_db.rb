require 'json'

class LanguagesDB
  @@instance = nil
  attr_reader :db
  def initialize
    datafile = Pathname(__FILE__).dirname.join('..', 'data', 'data.json')
    @db = JSON.parse(File.read(datafile)).map do |record|
      Language.new *record.values_at("Name", "Type", "Designed by")
    end
  end
  def self.instance
    @@instance ||= LanguagesDB.new
  end
  def self.size
    instance.db.size
  end
  def self.map &block
    instance.db.map do |e|
      block.call e
    end
  end
end

