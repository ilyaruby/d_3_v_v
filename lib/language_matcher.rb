class LanguageMatcher
  def self.relevant_words query_words, searchable_words
    query_words.map do |query_word|
      searchable_words.include?(query_word) ? query_word : false
    end
  end
  def self.relevance text_query, language
    query = LanguagesSearchQuery.new text_query

    query_matches_max = query.word_match_queries.count + query.exact_match_queries.count
    relevance_per_match = 1.0 / query_matches_max

    name_words = language.name.split(' ').map(&:downcase)
    types_words = language.types.split(',').map {|d| d.split(' ')}.flatten.map(&:downcase)
    designers_words = language.designers.split(',').map {|d| d.split(' ')}.flatten.map(&:downcase)
    combined_words = name_words + types_words + designers_words

    calculated_relevance = 0

    calculated_relevance += self.relevant_words(query.word_match_queries, combined_words).inject(0) do |a, matches|
      a += matches ? relevance_per_match : 0
    end

    self.relevant_words(query.negative_match_queries, combined_words).each do |matches|
      return LanguageRelevance.new language, 0.0 if matches
    end

    calculated_relevance += self.relevant_words(query.exact_match_queries, combined_words).inject(0) do |a, matches|
      a += matches ? relevance_per_match : 0
    end

    LanguageRelevance.new language, calculated_relevance
  end
end
