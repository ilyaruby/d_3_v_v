class Language
  attr_reader :name, :types, :designers
  def initialize name, types, designers
    @name, @types, @designers = name, types, designers
  end
  def relevance query
    LanguageMatcher.relevance(query, self)
  end
end

