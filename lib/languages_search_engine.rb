class LanguagesSearchEngine
  RelevanceMin = 0.6
  def self.db_accessible?
    LanguagesDB.size > 0
  end
  def self.all
    LanguagesDB.map do |language|
      language
    end
  end
  def self.search query
    LanguagesDB.map do |language|
      language.relevance(query)
    end.select do |language_relevance|
      language_relevance.precision > LanguagesSearchEngine::RelevanceMin
    end.sort_by do |language_relevance|
      language_relevance.precision
    end.reverse
  end
end
