class LanguagesSearchQuery
  attr_reader :text_query, :exact_match_queries, :word_match_queries, :negative_match_queries
  def initialize text_query
    @text_query = text_query.dup # this matters, because we will change it later
    extract_exact_matches
    extract_negative_matches
    extract_word_matches
    extract_word_matches
  end

  private

  def extract_negative_matches
    @negative_match_queries = []
    while match = extract_negative_match do
      @negative_match_queries << match
    end
  end

  def extract_negative_match
    match = @text_query.slice!(/\-[^ ]*/) #non-greedy first-match "..." extraction
    return nil unless match
    match.gsub(/^-/, '').downcase
  end

  def extract_exact_matches
    @exact_match_queries = []
    while match = extract_exact_match do
      @exact_match_queries << match
    end
  end

  def extract_exact_match
    match = @text_query.slice!(/\"[^\"]*\"/) #non-greedy first-match "..." extraction
    return nil unless match
    match.gsub('"', '').downcase
  end

  def extract_word_matches
    @word_match_queries = @text_query.split(' ').reject(&:empty?).map(&:downcase)
  end
end
