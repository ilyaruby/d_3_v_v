class LanguageRelevance
  attr_reader :language, :precision
  def initialize language, precision
    @language, @precision = language, precision
  end
end
